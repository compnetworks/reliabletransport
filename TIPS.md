[TOC]

1. To run the server yourself, simply run [ChannelEmulator](https://bitbucket.org/compnetworks/reliabletransport/src/master/ChannelEmulator.java) and the server will by default run on `localhost:8888` with the same default channel parameters as our supported public server, and the server further supports the following command-line arguments `java ChannelEmulator [-P|-PORT port] [-L|-LOSS lossrate] [-D|-DELAY delay_secs] [-D|-DELAY_DEV_RATIO deviation_to_delay_ratio] [-C|-CORRUPTION corruption_probability]`.

2. **Reminders** that seem to be easily forgotten by some students:
	1. Re-ordering of packets is to be expected as the delays have variance.
	2. UDP is not reliable, so it is possible for even control packets to get dropped, not just relayed data packets that are explicitly mangled by the server.
	3. A period possibly surrounded by whitespace alone in a datagram by itself switches from data relay mode to control mode, so make sure your protocol handles an occurrence of such a pattern in the data stream correctly.
	4. "Correctness first" is a principle that can not be overemphasized, so here it is again.
	5. Test your client with a variety of data transfers under different harsh channel parameter settings.
	6. `stdout` is the default output stream on the receiver end if an output file is not specified, so if you use `stdout` print statements to help debug your code (not a good development practice anyway), either make sure to have an option to easily turn them off before submission or print to `stderr` instead.
	7. In network or file I/O, all data is bytes -- bytes in, bytes out -- so if you find yourself doing any character encoding/decoding with the payload, you are probably doing it without understanding why you are doing it.
	
3. The contents of standard input or the input file as appropriate must be transferred exactly as-is, so you may need to escape control characters or commands.

4. **The glass is half empty**, no question about it. At any point in your code, ask yourself what would happen if a packet being sent or expected to be received got lost 99% of the time or got delivered at an arbitrary future time. If your code would hang forever or, worse, violate reliable bytestream semantics, you have a problem on your hands.

5. **Threads are your friends**: Typical socket API methods are blocking, so it is trickier to write an efficient single-threaded program that reads from a socket given that the datagram it is expecting to read may never arrive, so at least having separate sender and receiver threads is a good idea. The alternative to multithreading or static timeouts is to use only socket API methods that are not forever-blocking in your language, for example, using timeouts that will cause socket methods to eventually unblock, but hardcoded timeout values are less efficient and inelegant compared to threads.

6. **Autograder**:
	1. The autograder's tests and reported scores are not final and are provided to help nudge you in the right direction, but they are NOT meant to be
		1. *complete* (in that it is not meant to detect all of your bugs)
		2. or *sound* (in that it may assign you a very low score because, say, because it couldn't find your downloaded file at the expected place even though most of the rest of your code may be correct)
		3. or accurate (in that your final score may differ from what you see last because we may re-run your code multiple times or because the weighting of tests may be changed somewhat based on overall class performance).
		
	2. In grading and leaderboard presentation, correctness will be prioritized over speed, so for example, a submission with an [accuracy, time] score of [14, 250] will be adjudged as better than say [12, 100], but [12,150] is better than [12, 200].
	3. The above guidance means that you can and should continue to fix any correctness issues first as that is the best way to improve your correctness+speed evaluation.
	4. The autograder tests may time out and fail even on some tests if your client is unreasonably slow even if it is (or, rather, would have eventually been) correct, so choose timeout values judiciously in your design. If you are 100% confident that speed is the sole reason your client is timing out on some test (we can't know for sure), you can request a manual regrade post-deadline. Gradescope's Docker resources available to us limit the current test timeout values, but we do think they should be more than enough for even stop-and-wait protocol designs.

7. **FAQ**:
	1. *Is there something special about the test files? I swear I've thoroughly tested my program locally on files large and small and it transfers all of them correctly but the autograder says "_Contents of transmitted and received files differ_" on some tests*.
		
		**A**: There is nothing special about any file for data transfer purposes. All files are bytes. Bytes are bytes are bytes. If we told you the first file's contents are simply "Hello, how's it going?"; or we told you that they are the first 42 bytes of the `redsox.jpg` file from the previous project, how does this information help your protocol design in any way? It doesn't. If the autograder indicates a content mismatch, your transport protocol is incorrect, so you  need to (1) have an on-paper argument for the correctness of your design; and (2) test your implementation of the design more rigorously. 

	2. *How aggressive are the channel parameters in the tests?* 
		
		**A**: The honest answer to this question is that it is simply irrelevant for correctness. The mechanisms needed for correctness are the same irrespective of the whether some channel evilness parameter value is 0.001 or 0.999.
	
	3. *Why does the autograder time out on my code?* 
		
		**A**: We don't really know, but here are couple common cases to consider: (1) If the autograder times out more or less on all files, you probably have some thread waiting forever on an event that will never occur. (2) If the autograder times out only on the later tests (with larger files and harsher parameters), it's also possible that you are runnning into the "unreasonable slownness" problem in tip 6.4 above.  
	
	4. *Private post: I've been struggling to debug my code for a long time. Can you look at my code and tell me what I'm doing wrong?*
		
		**A**: Probably not, it's very difficult for anyone that's not you to debug your code. Instead (1) rephrase your question by concisely describing your design first as the majority of issues are in the design as opposed to the implementation; (2) use a stop-and-wait design prioritizing correctness first, not a pipelined design, as the former is easier to implement and debug. Once we've established your design is okay, we can then try to begin to guess what implementation mistakes you might be making.
	
	5. *How can I guarantee that both sender and receiver gracefully terminate because both know that the other knows that the other knows that... [ad infinitum]?"* 
		
		**A**: You can't because that is known to be impossible. Look at how TCP strikes a compromise that is good enough in practice. Termination is a *liveness* (=performance) concern, not a safety concern  (=correctness), so if either your sender or receiver on rare occasions doesn't terminate gracefully, the autograder will just kill them upon its timeout but it should still find the file transfer completed correctly.
	
	6. *How can I test with standard input and output (stdin/stdout) instead of files?* 
		
		**A**: Standard input stream is almost identical to a file input stream. Use a unix command like `python ChatClientSender.py -s server -p port < inputFile` (or equivalently `cat inputFile | python ChatClientSender.py -s server -p port`) at the sender and similarly `python ChatClientReceiver.py -s server -p port > outputFile` and perform `diff inputFile outputFile`. You can also hand-type or cut-paste text on the console and manually check if it's small-sized text. The only difference between standard and file input stream is that with manual typing on the console, EOF will never be reached, so your program won't have a termination signal, but we will never test your program manually like that for termination (but may perform other manual correctness checks if we deem it warranted).
	

8. More TBA here based on class questions. 
	