[TOC]

# Overview #

In this assignment, you will implement a reliable transport protocol for a messaging application. You are assumed to have prior familiarity using UDP and TCP sockets to write simple network application programs. This assignment asks you to pretend that TCP does not exist and implement reliable transport yourself on top of UDP.

Your goal is to write (possibly identical) UDP-based sender and receiver messaging client programs that communicate through a messaging server we maintain *in any language of your choice*. (In fact, your sender and receiver programs could even be in two separate languages, but you probably don't want to go that adventurous!) You can run the messaging server yourself on your local machine and that is the recommended option. You can also use the common messaging server we provide. It is strongly recommended that your client's transport protocol implementation proceed in the following steps emphasizing correctness before speed:

* Goal A: ensure reliable, in-order delivery of an input stream of text;
* Goal B: make your reliable transport protocol *fast* using pipelining.

This assignment comes equipped with an autograder. The following sections describe the messaging server protocol and sample code to play with its features; goal, design constraints and deliverables; submission instructions; and tips and FAQs about the design, implementation, and the autograder.


# Messaging server protocol #

The messaging server implements an *unreliable* channel much like paths in a datagram network like the Internet. We maintain a public messaging server running on `<date.cs.umass.edu, 8888>` waiting for UDP datagrams. Because of new UMass and CICS firewalling restrictions, the `date` server can only be accessed from within UMass. You can also run the [messaging server](https://bitbucket.org/compnetworks/reliabletransport/src/master/ChannelEmulator.java) yourself, which is the recommended option as you can clearly see from the server logs what is happenning. The server can do one of exactly three things with any datagram it receives:

1. process control commands and return response messages;
2. relay messages to another client;
3. echo messages (the default option).
	
	
## Playing with the protocol ##
You can use the unix command-line utility `netcat` (or this client [UDPTelnet.java](https://bitbucket.org/compnetworks/sockets/src/master/sockets-java/UDPTelnet.java)) to play and familiarize yourself with the messaging server protocol. Using either of those two (e.g., `nc -u server port`) try out the interactions described below, wherein the first line is what you type on the standard input prompt and the second is the response sent by the server. Single-terminal interactions are shown in one code block, two-terminal interactions are shown in two separate code blocks, and received response messages are shown in bold font only to distinguish them from sent messages.

By default, the server will simply echo messages back to the sender.
```
Hello there
```
**```Hello there```**


The control command `NAME` (case-sensitive) allows you to assign an arbitrary alphanumeric word without white spaces as your chat handle. All success responses will begin with `OK` and will be exactly one line long. The server will ignore all leading and trailing whitespaces while trying to interpret messages as control commands.
```
NAME Superman
```
**```OK Hello Superman```**

Now open a second terminal and use the NAME command similarly on it as follows.
```
NAME Batwoman
```
**```OK Hello Batwoman```**

At this point, the `LIST` control command on either terminal should show you the list of registered participants in the chat room like below.
```
LIST 
```
**```OK LIST = Batwoman/127.0.0.1:51970 Superman/127.0.0.1:61496```**

The `CONN` control command allows a client to start talking to another client as shown below. From this point onward, the messaging server will relay all messages from `Superman` (on the first terminal) to `Batwoman` (on the second terminal). Note that `CONN` sets up a unidirection relay, i.e., messages sent by `Batwoman` will not be relayed to `Superman` (unless `Batwoman` sends a `CONN Superman` control command to the server).
```
CONN Batwoman
```
**```OK Relaying to Batwoman at /127.0.0.1:51970```**

Instead of `CONN Superman`, the server also allows you to specify an `IP:port` combination like below. This option will be handy in case someone else hijacks your `NAME` in the public chat room by announcing it as theirs. The `NAME` command is provided for the convenience of not having to type IP addresses and port numbers. Make sure to pick two names that are unlikely to be used by anyone else in order to prevent inadvertent connection hijacking while using our public messaging server.
```
CONN 127.0.0.1:51970
```
**```OK Relaying to Batwoman at /127.0.0.1:51970```**

At this point, `Superman` can start chatting with his buddy `Batwoman` across the two different terminals using the relay as set up above.
```
Hello Batwoman, how goes?	
```
-------------
**
```
Hello Batwoman, how goes?
```
**

Sending a period '.' (without the quotes) and possibly whitespace alone in a datagram acts as a control command to stop the relaying set up above.
```
.
```
**```OK Not relaying```**

When the server is in relay mode, it will not echo messages back to the sender and will only relay them to the relay destination. It will also not accept any control messages like `NAME` or `CONN` above and simply treat them as data messages to be relayed to the relay destination. You have to use the '.' command to get back to the default control/echo mode.

The `QUIT` control command clears all state created by clients using control commands like `NAME, CONN`, etc. Please use `QUIT` gracefully before exiting. Otherwise, a garbage collector will delete all client state after about 30 minutes of inactivity, but if too many users all use the server at the same time (a concern only with using the common public server), the server could run out of memory and clear client state even earlier.

```
QUIT
```
**```OK Bye```**

## Channel characteristics ##

The big catch is that the server uses only UDP and might drop packets. Control and echo messages as above might get dropped if you have a poor network connection, but relay messages can additionally get dropped because the server is designed to be "mean". The relay channel has `LOSS, DELAY, DELAY_DEV_RATIO, and CORRUPTION` parameters that specify how often the channel can lose, delay, or corrupt packets. You can change these for testing your client under different (potentially harsh) conditions as below. All control commands to modify the channel parameters should be sent in the format `CHNL <parameter_name> <parameter_value>` as exemplified below.

```
CHNL LOSS 0.2
```
**```OK CHNL LOSS 0.2```**

You can also set multiple parameters as below.

```
CHNL LOSS 0.2 DELAY 0.2 CORRUPTION 0.01
```
**
```OK CHNL LOSS 0.1 DELAY 0.1 DELAY_DEV_RATIO 1.0 CORRUPTION 0.01```**

The `LOSS` parameter indicates the probability of a packet being lost. The `DELAY` parameter is the delay in seconds. The `CORRUPTION` parameter is the probability of exactly one byte getting corrupted, i.e., randomly switched to a different byte, in each 100-byte block in the datagram. The `DELAY_DEV_RATIO` specifies the ratio of the delay deviation and the average delay, e.g., a delay of 0.2 with a delay_dev_ratio of 0.5 means the delay is 200 milliseconds plus/minus 0-to-100 milliseconds.

The default values of `LOSS, DELAY, DELAY_DEV_RATIO`, and `CORRUPTION` are respectively 0.1, 0.1, 1.0, and 0.01. You can display the current channel parameters using the `CHNL PARAMS` control command.

```
CHNL PARAMS
```
**
```OK CHNL LOSS 0.1 DELAY 0.1 DELAY_DEV_RATIO 1.0 CORRUPTION 0.01```
**

# Goal, constraints, and deliverables #

Your goal is to implement a sender client program that ensures reliable, in-order delivery of data to a running instance of your (possibly identical) receiver program through the messaging server. For example, a transcript of a longer conversation may look like the following wherein the sender program is sending a message collected from its standard input to the receiver that is simply printing it to its standard output:

```
CONN Batwoman
```
**```OK Relaying to Batwoman at /127.0.0.1:51970```**
```
Hello Batwoman
How are you?
I just heard that Lincoln is set to give a speech in Gettysburg, PA, so we need to rush-deliver the speech transcript 
to Reuters stations across the country tonight so it makes it in time for the morning papers. Here is the transcript 
of the Gettysburg address:
Four score and seven years ago...
...
...
...
The end.
Did you get it okay?
```
-------------
**
```
Hello Batwoman
How are you?
I just heard that Lincoln is set to give a speech in Gettysburg, PA, so we need to rush-deliver the speech transcript 
to Reuters stations across the country tonight so it makes it in time for the morning papers. Here is the transcript 
of the Gettysburg address:
Four score and seven years ago...
...
...
...
The end.
Did you get it okay?
```
**

Your sender client program should be able to take input data from a file if one is specified as a command-line argument as detailed in "Submission instructions" below. We will only test your clients for unidirectional transfer, so it is up to you to decide whether the two clients are identical (like in TCP) or one is explicitly a sender and the other a receiver (making it easier like in Go-Back-N or Selective Repeat protocols in the textbook).

Clearly, the sender program can not just send the data to be transferred as-is like it appears in the transcript above as some of the data would be corrupted, lost or reordered, so your client needs to implement support for mechanisms for reliable transport. The receiver would need to send the sender some type of acknowledgments, so it would also need to use `CONN` to set up an unreliable relay channel in the opposite direction (not shown in the transcript above).

## Design constraints ##
Your code must respect the following constraints:

1. Your client programs can only use UDP.
2. Your client programs can only send/receive message to/from the messaging server.
3. The server will truncate any messages longer than 2048 bytes.
4. You can use a pipelined protocol with a window size of at most 16 segments as the server will drop further messages from a client after buffering that many segments.
5. You can not rely on `CHNL` commands in your submitted client for obvious reasons. They are only meant for you to test your code under various conditions, so we will disable all `CHNL` commands (including `CHNL PARAMS`) while evaluating your client.

You are free to design your own header format inside UDP messages. In this assignment, UDP is like IP and you reliable transport protocol's headers and payload will be encapsulated within a UDP datagram. You can use checksums, sequence numbers, timers, and any other mechanisms you deem needed and implement them in any language of your choice.

<!---
## Deliverables ##

The deliverables for Part A and Part B are similar except that Part A asks you to first focus on and get right correctness of reliable delivery, and then focus on speed in Part B:

**Part A deliverable: ** Sender and receiver client programs that correctly implement in-order reliable bytestream dellivery.

**Part B deliverable: ** Sender and receiver client programs that correctly implement in-order reliable bytestream dellivery as fast as possible.

Accordingly, the tests for Part A will focus only on correctness while the tests for Part B will focus on both correctness and speed. You are strongly encouraged to follow a correctness-first approach in your development before thinking about speed.
--->
# Submission instructions #

1. You must submit your (possibly identical) client programs named as `ChatClientSender.<appropriate-extension>` and `ChatClientReceiver.<appropriate-extension>`, e.g., `ChatClientSender.py` and `ChatClientReceiver.py` for python. We will test your client as follows. Suppose your main classes are `ChatClientSender.java` and `ChatClientReceiver.java`. Running the receiver followed by the sender on two different terminals (on possibly different machines) as follows should result in transferring the contents of the local file `filename1` correctly to the remote end and stored there as `filename2`.

	```
	java ChatClientSender -s server_name -p port_number -t filename1 filename2
	java ChatClientReceiver -s server_name -p port_number
	```
The client must support command-line arguments as above to specify the name and port number of the chat server and the input and output files. 

2. The default values of these paramters should be set to the server/port combination specified above, standard input, and standard output respectively, e.g., not specifying `filename` and `filename2` like below should transfer any standard input from the sender to the standard output of the receiver.

	```
	python ChatClientSender.py -s server_name -p port_number 
	python ChatClientReceiver.py -s server_name -p port_number
	```

3. You can use any programming language of your choice. For compiled languages that do not automatically discover and compile required helper files (like C/C++ for example), you need to include a Makefile.

4. Submit all your code files with your appropriately named client programs in the top-level directory. The autograder will simply look for the file `./ChatClientSender.<ext>` and `./ChatClientReceiver.<ext>`, so don't impose any sub-directory structure for any code files and don't zip/tar or otherwise package your files.


# Tips, FAQs, etc. #

See [here](https://bitbucket.org/compnetworks/reliabletransport/src/master/TIPS.md).